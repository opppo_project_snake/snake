#ifndef GAME_H
#define GAME_H

class AI;
class GameWindow;

#include <QMap>
#include <QObject>
#include <QVector>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QTimer>

#include "GameWindow.h"
#include "Snake.h"
#include "Map.h"


#include "RandomAI.h"
#include "SimpleAI.h"
#include "BombItem.h"
#include "ImmobilizedAI.h"
#include "UserControlledAI.h"
#include <QDebug>

class Game : public QObject
{
    Q_OBJECT
private:

    QTimer *timer;
    GameWindow *gameWindow;
    Map *map;

    float ItemSpawnCoef;
    float FoodSpawnCoef;
    float SnakeMovesPerSecond;
    int SnakeMovesBeforeTailCellDeath;

public:
    Game(QObject *parent = nullptr);
    ~Game();

    void showWindow();

    void start();
    void stop();
    void reset();
    void setMap(Map *map);

    void setSnakeAI(Snake *snake, AI *ai);

    const QVector<AI*>& getAIList();
    QVector<Item*> getAllItemTypes();
    QVector<Object*> getAllObjectTypes();

    Map* getMap();
    float getItemSpawnCoef();
    float getFoodSpawnCoef();
    float getSnakeMovesPerSecond();
    int getSnakeMovesBeforeTailCellDeath();
    int getDefaultAiIndex() const;
    QVector<QString> getMapList();

signals:
    void mapChanged(Map *map);

public slots:
    void setItemSpawnCoef(double coef);
    void setFoodSpawnCoef(double coef);
    void setSnakeMovesPerSecond(double moves);
    void setSnakeMovesBeforeTailCellDeath(double moves); // 0 = не удалять, целое число ходов.
    void loop();
};

#endif //GAME_H

