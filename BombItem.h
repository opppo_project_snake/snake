#ifndef BOMBITEM_H
#define BOMBITEM_H

#include "Item.h"

class BombItem : public Item
{
private:
    QPoint m_position;
    bool m_isDead = false;
public:
	BombItem();
	~BombItem();


	// 2 cells to left + central + 2 cells to right = field 5x5
    const int damageRadius = 3;

    const QString getName() const override;
    const Id getId() const override;
    const QPixmap getImage() const override;
    float getSpawnChance() const override;
    Entity* clone() const override;

    void collide(Snake *snake, Map *map) override;

    QPoint getPosition() override;
    void setPosition(QPoint position) override;

    void setIsDead(bool isDead) override;
    bool getIsDead() override;





};

#endif // BOMBITEM_H
