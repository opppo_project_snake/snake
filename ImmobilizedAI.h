#ifndef IMMOBILIZEAI_H
#define IMMOBILIZEAI_H

#include "AI.h"


class Map;
class Snake;

class ImmobilizedAI : public AI
{
private:

public:

    ImmobilizedAI();
    ~ImmobilizedAI();

    MoveDirection getNextMove(Snake *controllerSnake, Map *map) override;
    QString getName() const override;
};

#endif // IMMOBILIZEAI_H
