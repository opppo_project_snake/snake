#include "Entity.h"


Entity::Entity()
{
}

Entity::~Entity()
{
}

const Type Entity::getType() const
{
	Id id = getId();

	if (id >= ITEM_FIRST && id <= ITEM_LAST)
		return ITEM;
	if (id >= OBJECT_FIRST && id <= OBJECT_LAST)
		return OBJECT;
    return SNAKE;
}

