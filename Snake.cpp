#include <QPoint>
#include "Snake.h"

Snake::Snake(QString name)
{
    m_position = QPoint(0,0);
    m_isDead = false;

    m_color = Qt::green;
    m_name = name;
    tail.clear();

    m_currentScores = 0;

}

Snake::Snake(QString name, AI * ai)
{
    m_position = QPoint(0,0);
    m_isDead = false;

    m_color = Qt::green;
    m_name = name;
    tail.clear();

    m_currentScores = 0;

    m_Ai = ai;
}


Snake::~Snake()
{
    tail.clear();
    return;
}

const QString Snake::getName() const
{
    return m_name;
}

const Id Snake::getId() const
{
    return SNAKE_NPC;
}

const QPixmap Snake::getImage() const
{
    QPixmap item(":/img/Snake.png");

    return item;
}

QPoint Snake::getPosition()
{
    return m_position;
}

void Snake::setPosition(QPoint position)
{
    m_position = position;
}

const QPixmap Snake::getHeadImage() const
{
    QPixmap item(":/img/SnakeHead.png");

    return item;
}

bool Snake::getMustDie()
{
    return m_mustDie;
}

void Snake::setMustDie(bool mustDie)
{
    m_mustDie = mustDie;
}

bool Snake::getSnakeInTheHole()
{
    return m_snakeInTheHole;
}

void Snake::setSnakeInTheHole(bool snakeInTheHole)
{
    m_snakeInTheHole = snakeInTheHole;
}

void Snake::setName(QString name)
{
    m_name = name;
}

//bool Snake::snakesHeadWalk(QPoint newHead)
//{
//        if (getIsDead() == false && getSnakeInTheHole() == false);
//        {
//            newHead->insert(i.key(),i.key()->getPosition());    //В новую позицию головы понадобится вставить новую ячейку
//            switch (i.value()->getNextMove(i.key(), map))
//            {
//            case LEFT:  //LEFT
//                newValue = newHead->value(i.key());
//                newValue +=QPoint(-1,0);
//                newHead->insert(i.key(), newValue);
//                break;

//            case RIGHT:  //RIGHT
//                newValue = newHead->value(i.key());
//                newValue +=QPoint(1,0);
//                newHead->insert(i.key(), newValue);
//                break;

//            case UP:  //UP
//                newValue = newHead->value(i.key());
//                newValue +=QPoint(0,-1);
//                newHead->insert(i.key(), newValue);
//                break;

//            case DOWN: //DOWN
//                newValue = newHead->value(i.key());
//                newValue +=QPoint(0,1);
//                newHead->insert(i.key(), newValue);
//                break;

//            case STAND:
//                notMovingSnakes->insert(i.key());
//                break;
//            }
//        }

//        //Если змейка ушла за карту, позицию головы возвращаем назад, а змейку убиваем
//        if (newHead->value(i.key()) != i.key()->getPosition())
//        {
//            if (newHead->value(i.key()).x() >= map->getSizeX() || newHead->value(i.key()).x() < 0 ||
//                    newHead->value(i.key()).y() >= map->getSizeY() || newHead->value(i.key()).y() < 0)
//            {
//                bool cuted, dead; // нет значения?

//                //!Убить змею, убрать с карты. (В списке по идее оставить)

//                map->cutSnakeFrom(i.key()->getPosition(), cuted, dead);
//                i.key()->setIsDead(true); //Убиваем змею
//                i.key()->tail.clear(); //Удаляем всё с хвоста
//            }
//        }
//        if (i.key()->getIsDead() == true)
//        {
//            deaths++;
//        }

//}

QColor Snake::getColor()
{
    return m_color;
}

void Snake::setColor(QColor color)
{
    m_color = color;
}

QVector<QPoint> Snake::getTail()
{
    return this->tail;
}



int Snake::getCurrentScores()
{
    return m_currentScores;
}

void Snake::setCurrentScores(int currentScores)
{
    m_currentScores = currentScores;
}

bool Snake::isSnakeDead()
{
    return getIsDead() == true || getSnakeInTheHole() == true;
}

void Snake::setAi(AI * ai)
{
    m_Ai = ai;
}

AI *Snake::getAi()
{
    return m_Ai;
}

void Snake::collide(Snake *snake, Map *map)
{
    snake->setMustDie(true);
}

Entity *Snake::clone() const
{
    Snake *snake = new Snake(m_name);
    snake->setPosition(m_position);
    return snake;
}

void Snake::setIsDead(bool isDead)
{
    m_isDead  = isDead;
}

bool Snake::getIsDead()
{
    return m_isDead;
}
