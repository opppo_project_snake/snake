#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H
#include <QMainWindow>

class Game;
class MapGrid;
class Map;
class Snake;
class Entity;

#include "Game.h"

namespace Ui {
class GameWindow;
}

class GameWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameWindow(Game * t_game, QWidget *parent = nullptr);
    ~GameWindow();

    void animateCollision(Snake *snake, Entity *entity, float durationSeconds);
    void update();
    void setWinner(QVector<Snake*> snakes);

    QString getMapName() const;



private:
    Ui::GameWindow *ui;

    QMap <Snake*, int> snakesAiIndecies;

    MapGrid *mapGrid = nullptr;
    Game *game = nullptr;
    QString currentMapName;

    void setMap(Map *map);

signals:

public slots:
    void handleResults(const QString &);

    void on_start_button_clicked();
    void on_stop_button_clicked();
    void on_reset_button_clicked();

    void onMapChanged(Map *map);
    //void onStartClicked();
    //void onStopClicked();
    //void onResetClicked();

    void onMainSnakeSelected(int index);
    void onBindAIToSnake(int index);
    //void onOpenSnakeAIChoserDialog();
    Snake* getSnakeBySnakeName(QString name);
    void refreshSelectedSnakeAIGrid();
    void selectDropItem();
};

#endif // GAMEWINDOW_H
