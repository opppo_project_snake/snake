#include "WallObject.h"
#include "Snake.h"

WallObject::WallObject()
{
    m_position = QPoint(0,0);
    m_isDead = false;
}

const QString WallObject::getName() const
{
    return "Wall";
}


const Id WallObject::getId() const
{
    return WALL_OBJECT;
}


const QPixmap WallObject::getImage() const
{
    QPixmap item(":/img/WallObject.png");
    return item;
}

void WallObject::collide(Snake *snake, Map *map)
{
    //Q_UNUSED(map);
    snake->setMustDie(true);
}

Entity *WallObject::clone() const
{
	WallObject* wall = new WallObject();
    wall->setPosition(m_position);
    return wall;
}

QPoint WallObject::getPosition()
{
    return m_position;
}

void WallObject::setPosition(QPoint position)
{
    m_position = position;
}

void WallObject::setIsDead(bool isDead)
{
    m_isDead = isDead;
}

bool WallObject::getIsDead()
{
    return m_isDead;
}
