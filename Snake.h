#ifndef SNAKE_H
#define SNAKE_H
class AI;

#include <QPoint>
#include <QString>

#include "AI.h"
#include "Map.h"
#include "Entity.h"
#include "Score.h"


class Snake : public Entity
{
private:

    AI * m_Ai;
    QString m_name;
    QColor m_color;
    QPoint m_position;

    int m_currentScores;

    bool m_isDead = false;
    bool m_mustDie = false;
    bool m_snakeInTheHole = false;

public:

    QVector<QPoint> tail;

    explicit Snake(QString name, AI * ai);
    explicit Snake(QString name);
    ~Snake();

    const QString getName() const override;
    const Id getId() const override;
    const QPixmap getImage() const override;
    void collide(Snake *snake, Map *map) override;
    Entity* clone() const override;

    void setIsDead(bool isDead) override;
    bool getIsDead() override;

    QPoint getPosition() override;
    void setPosition(QPoint position) override;

    const QPixmap getHeadImage() const;

    bool getMustDie();
    void setMustDie(bool mustDie);

    bool getSnakeInTheHole();
    void setSnakeInTheHole(bool snakeInTheHole);

    void setName(QString name);

    QColor getColor();
    void setColor(QColor color);

    QVector<QPoint> getTail();

    int getCurrentScores();
    void setCurrentScores(int currentScores);
    void addCurrentScores();

    bool isSnakeDead();

    void setAi(AI * ai);
    AI * getAi();

};

#endif // SNAKE_H
