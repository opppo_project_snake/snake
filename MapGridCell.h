#ifndef MAPGRIDCELL_H
#define MAPGRIDCELL_H

#include "Entity.h"
#include "MapGrid.h"
#include "Snake.h"
#include <QPaintEngine>

#include <QLabel>
#include <QPixmap>
#include <QApplication>
#include <QMouseEvent>


class MapGridCell : public QLabel
{
    Q_OBJECT
private:
    QPoint coords;

public:
    MapGridCell(MapGrid *mapGrid, QPoint coords, QSize size, Entity *entity);
    ~MapGridCell();

    void setImage(const QPixmap &image);

signals:
    void mouseLmbClicked(QPoint);
    void mouseRmbClicked(QPoint);

private:
    void mousePressEvent(QMouseEvent *event);
};

#endif // MAPGRIDCELL_H
