#include "Game.h"
#include "GameWindow.h"




Game::Game(QObject *parent) : QObject(parent)
{
    setMap(map->getDefaultMap());
    gameWindow = new GameWindow(this);

    SnakeMovesBeforeTailCellDeath = 0;
    ItemSpawnCoef = 0.5;
    FoodSpawnCoef = 0.5;
    SnakeMovesPerSecond = 1.f;

    timer = new QTimer(parent);
    timer->setInterval(1000);
    timer->setSingleShot(false);
    connect(timer, SIGNAL(timeout()), this, SLOT(loop()));
}

Game::~Game()
{
    delete gameWindow;
    delete map;
}

void Game::showWindow()
{
    gameWindow->show();
}

void Game::setMap(Map *map)
{
    this->map = map;
    const QVector<Snake*> &snakes = map->getSnakes();
    for (int i = 0; i < snakes.size(); ++i)
    {
        setSnakeAI(snakes[i], new RandomAI());
    }
    emit mapChanged(map);
}

QVector<QString> Game::getMapList()
{
    QTextStream in;
    QFile *inFile = new QFile("maps.txt");
    inFile->open(QIODevice::ReadOnly);
    in.setDevice(inFile);
    in.setCodec("UTF-8");

    QVector<QString> names;

    while(!in.atEnd()) {
        names.push_back(in.readLine(255));
    }

    inFile->close();
    return names;
}

const QVector<AI *> &Game::getAIList()
{
    static QVector<AI*> list;
    if (list.size() == 0)
    {
        list.push_back(new RandomAI());
        list.push_back(new SimpleAI());
        list.push_back(new UserControlledAI());
        list.push_back(new ImmobilizedAI());
    }
    return list;
}

QVector<Item *> Game::getAllItemTypes()
{
    QVector<Item*> items;
    items.append(new FoodItem());
    items.append(new BombItem());

    return items;
}

QVector<Object *> Game::getAllObjectTypes()
{
    QVector<Object*> objects;
    objects.append(new WallObject());
    objects.append(new HoleObject());
    return objects;
}

Map* Game::getMap()
{
    return map;
}

float Game::getItemSpawnCoef()
{
    return this->ItemSpawnCoef;
}

float Game::getFoodSpawnCoef()
{
    return this->FoodSpawnCoef;
}

float Game::getSnakeMovesPerSecond()
{
    return this->SnakeMovesPerSecond;
}

int Game::getSnakeMovesBeforeTailCellDeath()
{
    return  this->SnakeMovesBeforeTailCellDeath;
}

int Game::getDefaultAiIndex() const
{
    return 0;
}

void Game::setItemSpawnCoef(double coef)
{
    ItemSpawnCoef = QString::number(coef).toFloat();
}

void Game::setFoodSpawnCoef(double coef)
{
    FoodSpawnCoef = QString::number(coef).toFloat();
}

void Game::setSnakeMovesPerSecond(double moves)
{
    SnakeMovesPerSecond = QString::number(moves).toFloat();
}

void Game::setSnakeMovesBeforeTailCellDeath(double moves) // 0 = не удалять, целое число ходов.
{
    SnakeMovesBeforeTailCellDeath = QString::number(moves + 0.01).toInt(); //Лень подключать math.h и нормальный round();
}

void Game::setSnakeAI(Snake *snake, AI *ai)
{
    snake->setAi(ai);
}

void Game::loop()
{
    //1. Ходят головы змеек
    //2. Во всех головах ищется еда
    //2.1. Если змейка съела еду, добавляем хвост дублёр
    //2.2. Если не съела, не добавляем
    //3. Умирают все врезавшиеся змейки в стены
    //4. Умирают все змейки от взрыва бомб
    //5.1 Умирают все змейки которые столкнулись головами
    //5.2 Умирают змейки от столкновения с телами других змеек (через карту) (+растущие змейки имеют реальный хвост)
    //6. Наконец сдвигается последняя ячейка на место первой
    //6.5. Проваливаются змейки в дырки. И новые добавляются к ним.
    //6.6. Отмирание змеиных ячеек.
    //7. Отрисовка происходящего на карте

    /////////////////////

    map->loop(FoodSpawnCoef, ItemSpawnCoef);
    if (map->thereIsWin())
    {
        gameWindow->on_stop_button_clicked();
        //    SetWinner
    }
}

void Game::start()
{
    timer->setInterval(1000/SnakeMovesPerSecond/*/movesPerSecondDefault*/);
    timer->start();

    //this->moveToThread(&GameThread);
    //GameThread.start();
}

void Game::stop()
{
    timer->stop();
}

void Game::reset() 
{
    timer->stop();    
    setMap(map->getDefaultMap());
}
