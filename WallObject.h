#ifndef WALLOBJECT_H
#define WALLOBJECT_H

#include "Object.h"

class WallObject : public Object
{
private:
    QPoint m_position;
    bool m_isDead = false;

public:
    WallObject();
    ~WallObject();



    const QString getName() const override;
    const Id getId() const override;
    const QPixmap getImage() const override;
    void collide(Snake *snake, Map *map) override;
    Entity* clone() const override;
    QPoint getPosition() override;
    void setPosition(QPoint position) override;

    void setIsDead(bool isDead) override;
    bool getIsDead() override;


};

#endif // WALLOBJECT_H
