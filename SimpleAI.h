#ifndef SIMPLEAI_H
#define SIMPLEAI_H

#include "AI.h"

class SimpleAI : public AI
{

private:
    int getCostsOfDirection(QPoint,MoveDirection,int*,Map*);
public:

    SimpleAI();
    ~SimpleAI();

    MoveDirection getNextMove(Snake *controllerSnake, Map *map) override;
    QString getName() const override;

    bool caseRight(QVector<QVector<Entity*> > *buf_field,  Map* map);
    bool caseLeft(QVector<QVector<Entity*> > *buf_field);
    bool caseUp(QVector<QVector<Entity*> > *buf_field);
    bool caseDown(QVector<QVector<Entity*> > *buf_field);

};

#endif // SIMPLEAI_H
