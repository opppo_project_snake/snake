#ifndef MAP_H
#define MAP_H

#include "Snake.h"

#include <QObject>
#include <QVector>
#include <QTextCodec>
#include <QMap>
#include <QSet>

#include <QFile>
#include <QTextStream>

#include "FoodItem.h"
#include "BombItem.h"
#include "HoleObject.h"
#include "WallObject.h"



class Map : public QObject
{
    friend class Game;
	Q_OBJECT

    int m_sizeX, m_sizeY;
    QVector<QVector<Entity*> > field;
    QVector<Snake*> snakes;

    QSet<Snake*> snakesUniqueConstraint;
    QMap<int, Item*> itemsTypesForGeneration;

    int deaths;

public:
	explicit Map(int sizeX, int sizeY);
	~Map();

    Map *getDefaultMap();

	const QString getName();
	const QString getId();

	const int getSizeX();
	const int getSizeY();

	const QVector<QVector<Entity*> >& getField();
	const Entity* getEntityAt(const QPoint &point);
	const Entity* getEntityAt(int x, int y);

	const QVector<Item*>& getItems();
	const QVector<Object*>& getObjects();
	const QVector<Snake*>& getSnakes();

	const QVector<Item*>& getItemsTypesForGeneration();
	void addItemTypeForGeneration(Item *item);
	void removeItemTypeForGeneration(Item *item);

	void resize(int newSizeX, int newSizeY);
	void setCellAt(int x, int y, Entity *newEntity);
	void setCellByEntity(Entity *newEntity);
	void setCellsBySnake(Snake *snake);
	bool addSnakeTailAt(Snake *snake, QPoint coords);

//	bool isSnakeExist(Snake* snake);
	bool isCellEmpty(QPoint coords);

	void cutSnakeFrom(QPoint coords, bool& cuttedAtLeast1, bool& wasFullyRemoved);
	bool cutSnakeTailFrom(QPoint coords);
	void clearCellAt(QPoint coords);
	void clearCellAt(int x, int y);
	void clearCellsBySnake(Snake *snake);

    int getTypeField(QPoint pointer);
    Map * loadMapFromFile(QString mapName);
    QVector<QPoint> getEmptyCoordsVector();

    bool loop(float FoodSpawnCoef, float ItemSpawnCoef);
    bool snakesHeadsWalk(QMap<Snake *, QPoint> *newHead, Snake * snake);
    void meeting(QMap<Snake *, QPoint> *newHead, Snake * t_snake);
    bool thereIsWin();
    void moveSnakeFull(QMap <Snake*, QPoint> *newHead, Snake * t_snake);
    bool createItems(float FoodSpawnCoef, float ItemSpawnCoef);
    void createEat(QVector<QPoint> * empty, float FoodSpawnCoef);
    void doMustDie(Snake * t_snake);

private:
	void removeEntityFromVectors(Entity *entity);
	void addEntityToVectors(Entity *entity);
	template<class T>
	void removeEntityFromVector(QVector<T> &vector, Entity *entity);

signals:
	void sizeChanged(int newSizeX, int newSizeY);
	void cellChangedAt(int x, int y, Entity *oldEntity, Entity *newEntity);
};

#endif // MAP_H
