#include "FoodItem.h"
#include "Snake.h"
#include "Entity.h"

FoodItem::FoodItem(){}
FoodItem::~FoodItem(){}

const QString FoodItem::getName() const
{
    return "Food";
}

const Id FoodItem::getId() const
{
    return FOOD_ITEM;
}

const QPixmap FoodItem::getImage() const
{
    QString fileName = QString().sprintf("%s%d%s", ":/img/FoodItem", qrand() % 3, ".png");
    QPixmap item(fileName);

    return item;
}

void FoodItem::collide(Snake *snake, Map *map)
{
    Q_UNUSED(map);
    if (!snake->tail.isEmpty())
    {
        QPoint cell = snake->tail.last();
        snake->tail.push_back(cell); //Добавляем в конец хвоста тот же хвост
    }
    else
        snake->tail.insert(0,snake->getPosition()); //Вставляем хвост на место головы

}

float FoodItem::getSpawnChance() const
{
	return 0.25f;
}

Entity *FoodItem::clone() const
{
	FoodItem *food = new FoodItem();
    food->setPosition(m_position);
    return food;
}

QPoint FoodItem::getPosition()
{
    return m_position;
}

void FoodItem::setPosition(QPoint position)
{
    m_position = position;
}

void FoodItem::setIsDead(bool isDead)
{
    m_isDead = isDead;
}

bool FoodItem::getIsDead()
{
    return m_isDead;
}
