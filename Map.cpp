#include "Map.h"
#include <qDebug>
Map::Map(int sizeX, int sizeY)
{
    m_sizeX = sizeX;
    m_sizeY = sizeY;
    resize(sizeX, sizeY);
    addItemTypeForGeneration(new BombItem);
    deaths = 0;
}

Map::~Map()
{
    field.clear();
    itemsTypesForGeneration.clear();
    snakes.clear();
}

Map *Map::getDefaultMap()
{
    Map* ret = new Map(12,9);

    Snake* s1= new Snake("Левая");
    s1->setPosition(QPoint(2,4));

    s1->tail << QPoint(1,4);
    s1->tail << QPoint(0,4);

    s1->setColor(Qt::cyan);
    ret->setCellsBySnake(s1);


    Snake* s2= new Snake("Правая");
    s2->setPosition(QPoint(9,4));

    s2->tail << QPoint(10,4);
    s2->tail << QPoint(11,4);

    s2->setColor(Qt::blue);
    ret->setCellsBySnake(s2);

    //		for (int i = 0; i < itemTypes.size(); ++i) {
    //			ret->addItemTypeForGeneration(itemTypes[i]);
    //		}

    return ret;
}


const int Map::getSizeX()
{
    return m_sizeX;
}

const int Map::getSizeY()
{
    return m_sizeY;
}

const Entity * Map::getEntityAt(const QPoint &point)
{
    return field[point.x()][point.y()];
}

const Entity * Map::getEntityAt(int x, int y)
{
    return field[x][y];
}

const QVector<QVector<Entity *> >& Map::getField()
{
    return field;
}

const QVector<Snake *>& Map::getSnakes()
{
    return snakes;
}

const QVector<Item *>& Map::getItemsTypesForGeneration()
{
    return *(new QVector<Item *>(itemsTypesForGeneration.values().toVector()));
}

void Map::addItemTypeForGeneration(Item *item) // не используется
{
    itemsTypesForGeneration[item->getId()] = item;
}

void Map::removeItemTypeForGeneration(Item *item)
{
    itemsTypesForGeneration.remove(item->getId());
}

void Map::resize(int newSizeX, int newSizeY)
{
    Entity *entity;
    snakes.clear();

    // Создаем новую карту на основе старой

    field.resize(newSizeX);
    for (int x = 0; x < newSizeX; ++x)
    {
        field[x].resize(newSizeY);
        for (int y = 0; y < newSizeY; ++y)
        {
            entity = field[x][y];
            if (entity != nullptr)
            {
                switch(entity->getType())
                {
                case ITEM :
                    break;

                case OBJECT :
                    break;

                case SNAKE :
                    if (entity->getPosition().x() == x && entity->getPosition().y() == y)
                    {
                        snakes.append((Snake*)entity);
                    }
                    else
                    {
                        field[x][y] = nullptr;
                    }
                    break;
                }
            }
        }
    }

    // Удаляем остатки змеек за пределами нового размера
    for (int i = 0; i < snakes.size(); ++i)
    {
        QVector<QPoint> &tail = snakes[i]->tail;
        for (QVector<QPoint>::Iterator it = tail.begin(); it != tail.end(); ++it)
        {
            if (it->x() < newSizeX && it->y() < newSizeY)
            {
                field[it->x()][it->y()] = snakes[i];
            }
            else
            {                                      //возможно стоит удалить голову и вырезать её из массива
                tail.erase(it, tail.end());
                break;
            }
        }
    }

    m_sizeX = newSizeX;
    m_sizeY = newSizeY;
    emit sizeChanged(newSizeX, newSizeY);
}

void Map::setCellAt(int x, int y, Entity *newEntity)
{
    Entity *oldEntity = field[x][y];
    field[x][y] = newEntity;
    if (!(oldEntity != nullptr && oldEntity->getType() == SNAKE && oldEntity->getPosition() != QPoint(x, y)))
    {
        removeEntityFromVectors(oldEntity);
    }

    addEntityToVectors(newEntity);
    emit cellChangedAt(x, y, oldEntity, newEntity);
}

void Map::setCellByEntity(Entity *newEntity)
{
    setCellAt(newEntity->getPosition().x(), newEntity->getPosition().y(), newEntity);
}

void Map::setCellsBySnake(Snake *snake)
{
    QPoint tailCell;
    setCellAt(snake->getPosition().x(), snake->getPosition().y(), snake);
    for (int i = 0; i < snake->tail.size(); ++i)
    {
        tailCell = snake->tail[i];
        setCellAt(tailCell.x(), tailCell.y(), snake);
    }
}

bool Map::addSnakeTailAt(Snake *snake, QPoint coords)
{
    bool wasTailCellAdded = false;
    if (getEntityAt(coords) == nullptr)
    {
        QPoint lastTailCell = snake->getPosition();
        if (snake->tail.size() > 0)
        {
            lastTailCell = snake->tail.last();
        }

        QPoint cellsDiff = lastTailCell - coords;
        if (qAbs(cellsDiff.x()) + qAbs(cellsDiff.y()) == 1)
        {
            wasTailCellAdded = true;
            snake->tail.push_back(coords);
            setCellAt(coords.x(), coords.y(), snake);
        }
    }

    return wasTailCellAdded;
}

//bool Map::isSnakeExist(Snake *snake)
//{
//	if (snakesUniqueConstraint.find(snake) != snakesUniqueConstraint.end())
//		return true;
//	return false;
//}

bool Map::isCellEmpty(QPoint coords)
{
    if (field[coords.x()][coords.y()] == nullptr)
        return true;
    return false;
}

void Map::cutSnakeFrom(QPoint coords, bool &cuttedAtLeast1, bool &wasFullyRemoved)
{
    cuttedAtLeast1 = false;
    wasFullyRemoved = false;

    if (getEntityAt(coords)->getType() == SNAKE)
    {
        Snake *snake = (Snake*)getEntityAt(coords);
        cuttedAtLeast1 = true;

        if (snake->getPosition() == coords)
        {
            clearCellsBySnake(snake);
            wasFullyRemoved = true;
        }
        else
        {
            cutSnakeTailFrom(coords);
        }

        if (wasFullyRemoved == true)
        {
            snake->setIsDead(true);//Убиваем змею
            snake->tail.clear(); //Удаляем всё с хвоста
        }
    }
}

bool Map::cutSnakeTailFrom(QPoint coords)
{
    bool wasCutted = false;
    if (getEntityAt(coords)->getType() == SNAKE)
    {
        Snake *snake = (Snake*)getEntityAt(coords);

        QVector<QPoint> &tail = snake->tail;
        QVector<QPoint>::Iterator cuttingPoint = nullptr;

        for (QVector<QPoint>::Iterator it = tail.begin(); it != tail.end(); ++it)
        {
            if (*it == coords)
            {
                cuttingPoint = it;
                wasCutted = true;
            }

            if (cuttingPoint != nullptr)
            {
                clearCellAt(*it);
            }
        }

        if (cuttingPoint != nullptr)
        {
            tail.erase(cuttingPoint, tail.end());
        }
    }
    return wasCutted;
}

void Map::clearCellAt(QPoint coords)
{
    clearCellAt(coords.x(), coords.y());
}


void Map::clearCellAt(int x, int y)
{
    setCellAt(x, y, nullptr);
}

void Map::clearCellsBySnake(Snake *snake)
{
    if (snake->tail.size() > 0)
    {
        cutSnakeTailFrom(snake->tail.first());
    }
    clearCellAt(snake->getPosition());
}



bool Map::snakesHeadsWalk(QMap<Snake *, QPoint> *newHead, Snake * snake)
{
    QPoint newValue;
    if (!snake->getIsDead())
    {
        newHead->insert(snake,snake->getPosition());    //В новую позицию головы понадобится вставить новую ячейку
        switch (snake->getAi()->getNextMove(snake, this))
        {
        case LEFT:  //LEFT
            newValue = newHead->value(snake);
            newValue +=QPoint(-1,0);
            newHead->insert(snake, newValue);
            break;

        case RIGHT:  //RIGHT
            newValue = newHead->value(snake);
            newValue +=QPoint(1,0);
            newHead->insert(snake, newValue);
            break;

        case UP:  //UP
            newValue = newHead->value(snake);
            newValue +=QPoint(0,-1);
            newHead->insert(snake, newValue);
            break;

        case DOWN: //DOWN
            newValue = newHead->value(snake);
            newValue +=QPoint(0,1);
            newHead->insert(snake, newValue);
            break;

        case STAND:
            //notMovingSnakes->insert(snake);
            break;
        }
    }

    //Если змейка ушла за карту, позицию головы возвращаем назад, а змейку убиваем
    if (newHead->value(snake) != snake->getPosition())
    {
        if (newHead->value(snake).x() >= this->getSizeX() || newHead->value(snake).x() < 0 ||
                newHead->value(snake).y() >= this->getSizeY() || newHead->value(snake).y() < 0)
        {
            bool cuted, dead; // нет значения?

            //!Убить змею, убрать с карты. (В списке по идее оставить)
            cutSnakeFrom(snake->getPosition(), cuted, dead);
        }
    }

    if (snake->getIsDead() == true)
    {
        deaths++;
    }

    if (thereIsWin() == true)
        return false;

    return true;
}

bool Map::thereIsWin()
{
    if ( deaths >= (snakes.count() -1))
    {
        QVector <Snake*> winnerSnakes;
        for (int q = 0; q < getSnakes().size(); q++)
        {
            if (getSnakes()[q]->getIsDead() == false)
            {
                winnerSnakes.append(this->getSnakes()[q]);
            }
        }
        return true;
    }
    return false;
}


int Map::getTypeField(QPoint pointer)
{
    return this->field[pointer.x()][pointer.y()]->getType();
}

void Map::meeting(QMap<Snake *, QPoint> *newHead, Snake * t_snake)
{
    int x,y;
    if (!t_snake->getIsDead())
    {

        x = newHead->value(t_snake).x();
        y = newHead->value(t_snake).y(); //Координаты головы змейки

        if (getField()[x][y] != nullptr) //Если есть объект в клетке - взаимодействуем с ним
        {
            if ((getField()[x][y]->getId()!=SNAKE_NPC)) //Совпадают с координатами змейки на карте
            {
                getField()[x][y]->collide(t_snake,this);
                        qDebug() << "1 Q" << endl;
             //   if (getField()[x][y]->getId()== WALL_OBJECT)
               // {
               //     newHead->insert(t_snake, t_snake->getPosition());
               // }
            }qDebug() << "2 Q" << endl;
            if(getField()[x][y]->getId()==SNAKE_NPC && newHead->value(t_snake) != t_snake->getPosition())
            {
                Snake *snake = (Snake*)getEntityAt(x,y);
                if(snake->getTail().size() < t_snake->getTail().size())
                {
                    //getField()[x][y]->collide(snake,this);
                    t_snake->collide(snake, this);
                }
                else
                {
                    getField()[x][y]->collide(t_snake,this);
                }

            }qDebug() << "3 Q" << endl;
        }
    }
}

void Map::moveSnakeFull(QMap <Snake*, QPoint> *newHead, Snake * t_snake)
{
    if (!t_snake->getIsDead() && newHead->value(t_snake) != t_snake->getPosition() && t_snake->getMustDie() == false)
    {
        if (t_snake->tail.size() > 0) //Если у змейки есть хвост
        {
            //Проверка на еду
            if (t_snake->tail.size() > 1)
            {
                if ( !(t_snake->tail.last() == (t_snake->tail[t_snake->tail.size()-2])) )
                {
                    setCellAt(t_snake->tail.last().x(), t_snake->tail.last().y(), nullptr); //Удаляем конец хвоста с карты
                }
            }
            else if ( !(t_snake->getPosition() == t_snake->tail.last()) )
            {   //Если змея не ела, отрубаем конец
                setCellAt(t_snake->tail.last().x(), t_snake->tail.last().y(), nullptr); //Удаляем конец хвоста с карты
            }
            QPoint oldHead = t_snake->getPosition() ;
            t_snake->setPosition(newHead->value(t_snake));
            t_snake->tail.removeLast(); //Удаляем с конца хвоста
            t_snake->tail.insert(0,oldHead); //Вставляем в начало хвоста
            setCellAt(newHead->value(t_snake).x(), newHead->value(t_snake).y(), t_snake); //Вставляем голову на место новой головы
            setCellAt(oldHead.x(), oldHead.y(), t_snake); //Вставляем тело на место головы
        }
        else
        {
            if (((Snake*)getEntityAt(t_snake->getPosition().x(), t_snake->getPosition().y())) == t_snake)
                setCellAt(t_snake->getPosition().x(), t_snake->getPosition().y(), nullptr); //Удаляем старое отображение головы
            t_snake->setPosition(newHead->value(t_snake));
            setCellAt(newHead->value(t_snake).x(), newHead->value(t_snake).y(), t_snake); //Вставляем голову на новое место
        }
    }
}

QVector<QPoint> Map::getEmptyCoordsVector()
{
    QVector<QPoint> empty;
    for (int imap = 0; imap < this->getSizeX(); imap++)
    {
        for (int jmap = 0; jmap < this->getSizeY(); jmap++)
        {
            if (this->getField()[imap][jmap] == nullptr)
                empty << QPoint(imap,jmap);
        }
    }
    return empty;
}

bool Map::loop(float FoodSpawnCoef, float ItemSpawnCoef)
{
    QMap <Snake*, QPoint> newHead;

    QVectorIterator<Snake*> i(snakes);
    i.toFront();
    while (i.hasNext())
    {
        snakesHeadsWalk(&newHead, i.next());
        meeting(&newHead, i.peekPrevious());
        moveSnakeFull(&newHead, i.peekPrevious());

        if (i.peekPrevious()->getMustDie() == true && i.peekPrevious()->getIsDead() == false) //Реализуем все mustDie
        {
            bool cuted, dead;
            //!Убить змею, убрать с карты. (В списке по идее оставить)
            cutSnakeFrom(i.peekPrevious()->getPosition(),cuted,dead);
        }
    }
    createItems(FoodSpawnCoef, ItemSpawnCoef);
    return true;
}

bool Map::createItems(float FoodSpawnCoef, float ItemSpawnCoef)
{
    //Генерация вещей из списка доступных
    //rand() для числа
    //float randomNum = ((float)(qrand()%100))/100;
    //Набираем вектор пустых ячеек на карте с NULL

    QVector<QPoint> empty = getEmptyCoordsVector();

    if(empty.isEmpty())
        return true;

    createEat(&empty, FoodSpawnCoef); //Генерируем еду

    //Выбираем каждый элемент из map->getItemsTypesForGeneration()
    for (int loop = 0; loop < getItemsTypesForGeneration().size(); loop++)
    {
        if (getItemsTypesForGeneration()[loop]->getSpawnChance()*ItemSpawnCoef >= (((float)(qrand()%100))/100) && empty.size() > 0)
        {
            //Генерируем в пустое место
            int num = qrand()%empty.size();
            if (getItemsTypesForGeneration()[loop]->getId() == BOMB_ITEM) //Проверка ID
            {
                BombItem *Bomb = new BombItem();
               // Bomb->setPosition(QPoint(, empty[num].y()));
                Bomb->getPosition().setX(empty[num].x());
                           Bomb->getPosition().setY(empty[num].y());
                setCellAt(empty[num].x(), empty[num].y(), Bomb);
            }
            //else if (map->getItemsTypesForGeneration()[loop]->getId() == OTHER_ITEM) //Если появятся другие предметы, нужно будет добавить сюда код
            //Обновляем вектор пустых точек (удаляя старый из него по номеру в векторе)
            empty.removeAt(num);
        }
    }

    //Если пустого места нет - вектор сайз равен нулю, то скипаем генерацию объекта
    //Генерим до конца все объекты на карту
    //рисуем всё новое

    return true;
}

void Map::createEat( QVector<QPoint> * empty, float FoodSpawnCoef)
{
    FoodItem *for_generation = new FoodItem();
    if (for_generation->getSpawnChance()*FoodSpawnCoef >= (((float)(qrand()%100))/100) && empty->size() > 0) //!for_generation <=> map->getItemsTypesForGeneration()[0]
    {
        //Генерируем в пустое место
        int num = qrand()%empty->size();
        for_generation->getPosition().setX(empty->value(num).x());
        for_generation->getPosition().setX(empty->value(num).y());
        setCellAt(empty->value(num).x(), empty->value(num).y(), for_generation);

        //Обновляем вектор пустых точек (удаляя старый из него по номеру в векторе)
        empty->removeAt(num);
        //map->getItems().append(for_generation);

    }
}

void Map::doMustDie(Snake *t_snake)
{
    if (t_snake->getMustDie() == true && t_snake->getIsDead() == false)
    {
        bool cuted, dead;
        //!Убить змею, убрать с карты. (В списке по идее оставить)
        cutSnakeFrom(t_snake->getPosition(),cuted,dead);
    }
}


void Map::addEntityToVectors(Entity *entity)
{
    if (entity != nullptr)
    {
        if (entity->getType()== SNAKE )
        {
            Snake *snake = (Snake*)entity;
            auto it = snakesUniqueConstraint.find(snake);
            if (it == snakesUniqueConstraint.end())
            {
                snakes.append((Snake*)entity);
                snakesUniqueConstraint.insert(snake);
            }
        }
    }
}

void Map::removeEntityFromVectors(Entity *entity)
{
    if (entity != nullptr)
    {
        //        field[entity->getPosition().x()].removeAt(entity->getPosition().y());
        if (entity->getType()== SNAKE )
        {
            removeEntityFromVector<Snake*>(snakes, entity);
            snakesUniqueConstraint.remove((Snake*)entity);
        }
    }
}

template<class T>
void Map::removeEntityFromVector(QVector<T> &vector, Entity *entity)
{
    for (int i = 0; i < vector.size(); ++i)
    {
        if (vector[i]->getPosition() == entity->getPosition())
        {
            vector.removeAt(i);
            break;
        }
    }
}
