#include "HoleObject.h"
#include "Snake.h"
#include "Map.h"


HoleObject::HoleObject()
{
    m_position = QPoint(0,0);
    m_isDead = false;
}


HoleObject::~HoleObject()
{

}


const QString HoleObject::getName() const
{
    return "Hole";
}


const Id HoleObject::getId() const
{
    return HOLE_OBJECT;
}


const QPixmap HoleObject::getImage() const
{
    QPixmap item(":/img/HoleObject.png");

    return item;
}

void HoleObject::collide(Snake *snake, Map *map)
{
    snake->setSnakeInTheHole(true);
    if (snake->tail.size() > 0) //Делаем один ход
    {
        QPoint oldHead = snake->getPosition();

        snake->setPosition(QPoint(this->getPosition()));

        map->setCellAt(snake->tail.last().x(), snake->tail.last().y(), nullptr); //Удаляем конец хвоста с карты
        map->setCellAt(oldHead.x(), oldHead.y(), snake); //Вставляем перёд на место старой головы
        snake->tail.removeLast(); //Удаляем с конца хвоста
        snake->tail.insert(0,oldHead); //Вставляем в начало хвоста
    }

    if (snake->getSnakeInTheHole() == true && snake->getIsDead() == false) //Змейка в дырке, но не умерла
    {
        if (snake->tail.size() > 0)
        {
            map->setCellAt(snake->tail.last().x(), snake->tail.last().y(), nullptr); //Удаляем ячейку хвоста с карты
            snake->tail.removeLast(); //Засасываем одну ячейку под землю русскую.
            if (snake->tail.size() == 0) //Наконец убиваем нашу змею
                snake->setMustDie(true);
        }
        else //Если змея и так была без хвоста
            snake->setMustDie(true);
    }
}

Entity *HoleObject::clone() const
{
	HoleObject *hole = new HoleObject();
    hole->setPosition(m_position);
    return hole;
}

QPoint HoleObject::getPosition()
{
    return m_position;

}

void HoleObject::setPosition(QPoint position)
{
    this->m_position = position;

}

void HoleObject::setIsDead(bool isDead)
{
    m_isDead = isDead;
}

bool HoleObject::getIsDead()
{
    return m_isDead;
}
