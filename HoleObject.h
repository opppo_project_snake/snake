#ifndef HOLEOBJECT_H
#define HOLEOBJECT_H

#include "Object.h"

class HoleObject : public Object
{

    QPoint m_position;
    bool m_isDead = false;

public:
    HoleObject();
    ~HoleObject();


    const QString getName() const override;
    const Id getId() const override;
    const QPixmap getImage() const override;
    void collide(Snake *snake, Map *map) override;
    Entity* clone() const override;
    QPoint getPosition() override;
    void setPosition(QPoint position) override;

    void setIsDead(bool isDead) override;
    bool getIsDead() override;


};

#endif // HOLEOBJECT_H
