#ifndef FOODITEM_H
#define FOODITEM_H

#include "Item.h"

class FoodItem : public Item
{
private:
    QPoint m_position = QPoint(0,0);
    bool m_isDead = false;

public:
    FoodItem();
    ~FoodItem();

     const QString getName() const override;
     const Id getId() const override;
     const QPixmap getImage() const override;
     void collide(Snake *snake, Map *map) override;
     float getSpawnChance() const override;
     Entity* clone() const override;

     QPoint getPosition() override;
     void setPosition(QPoint position) override;

     void setIsDead(bool isDead) override;
     bool getIsDead() override;


};

#endif // FOODITEM_H
