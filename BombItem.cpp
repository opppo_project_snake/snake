#include "BombItem.h"
#include "Snake.h"
#include "Map.h"
#include <QDebug>
BombItem::BombItem()
{
    m_position = QPoint(0,0);
    m_isDead = false;
}

BombItem::~BombItem()
{
    return;
}

const QString BombItem::getName() const
{
    return "Bomb";
}

const Id BombItem::getId() const
{
    return BOMB_ITEM;
}

const QPixmap BombItem::getImage() const
{
    QPixmap item(":/img/BombItem.png");

    return item;
}

void BombItem::collide(Snake *snake, Map *map)
{

    //map->clearCellAt(snake->position.x(),snake->position.y());
    //qDebug() << "POS X:" << snake->position.x() << " POS Y:" << snake->position.y();
    //Взрыв всех вещей в радиусе damageRadius
    //Взрыв змеек, голов и их частей
    int x = 0, y = 0;

    for (int i = 0; i <= damageRadius; i++)
        {
            for (int j = 0; j < i*2+1; j++)
            {
                x = m_position.x()-i+j;
                if (x<0)
                    x=0;
                else if (x>=map->getSizeX())
                    x=map->getSizeX()-1;
                y = m_position.y()-i+damageRadius;
                if (y<0)
                    y=0;
                else if (y>=map->getSizeY())
                    y=map->getSizeY()-1;
                if (!snake->getTail().contains(QPoint(x,y)) && !map->isCellEmpty(QPoint(x,y)))
                {
                    if ((map->getField()[x][y]->getId() == SNAKE_NPC))
                    {
                        bool cuted, isDead = false;
                        Snake *snake = (Snake*)map->getEntityAt(QPoint(x,y));
                        map->cutSnakeFrom(QPoint(x,y),cuted,isDead);
    //                    if (isDead)
    //                    {
    //                        snake->setIsDead(true);
    //                        snake->tail.clear(); //Удаляем всё с хвоста
    //                    }
                    }
                    else
                    {
                        map->clearCellAt(x,y);
                    }
                }
            }
        }


//    for (int i = 0; i <= damageRadius; i++)
//    {
//        for (int j = 0; j < i*2+1; j++)
//        {
//            x = m_position.x()+damageRadius/2 - i;

//            if (x<0)
//                x=0;
//            else if (x>=map->getSizeX())
//                x=map->getSizeX()-1;
//            y = m_position.y()+damageRadius/2 - j;
//            if (y<0)
//                y=0;
//            else if (y>=map->getSizeY())
//                y=map->getSizeY()-1;
//            qDebug() << " i = " << i << "  " << " j = " << j << "  " << " x = " << x << "  " << " y = " << "  " << y;
//            if (!snake->getTail().contains(QPoint(x,y)) && !map->isCellEmpty(QPoint(x,y)) && !(snake->getPosition() == QPoint(x,y)))
//            {
//                if ((map->getField()[x][y]->getId() == SNAKE_NPC))
//                {
//                    bool cuted, isDead = false;
//                   // Snake *snake = (Snake*)map->getEntityAt(QPoint(x,y));
//                    map->cutSnakeFrom(QPoint(x,y),cuted,isDead);
//                }
//                else
//                {
//                    map->clearCellAt(x,y);
//                }
//            }
//        }
//    }
    qDebug() << "|qwewqe|;";
}

QPoint BombItem::getPosition()
{
    return m_position;
}

void BombItem::setPosition(QPoint position)
{
    m_position = position;
}

void BombItem::setIsDead(bool isDead)
{
    m_isDead = isDead;
}

bool BombItem::getIsDead()
{
    return  m_isDead;
}

float BombItem::getSpawnChance() const
{
    return 0.15f;
}

Entity *BombItem::clone() const
{
    BombItem *bomb = new BombItem();
    bomb->setPosition(m_position);
    return bomb;
}
